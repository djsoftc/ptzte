import request from '@/utils/request'

export function getCodeList(id) {
  return request({
    url: '/codelist/getCodelist',
    method: 'get',
    params: { id }
  })
}
