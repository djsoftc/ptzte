import Mock from 'mockjs'
import { param2Obj } from '@/utils'
import { getCodelists } from './codelist.js'
const count = 5

function randomdata(name) {
  let value = '@increment'
  if (name.indexOf('地址') !== -1) {
    value = '@county(true)'
  }
  if (name.indexOf('电子邮件') !== -1) {
    value = '@email'
  }
  if (name.indexOf('人') !== -1) {
    value = '@cname'
  }
  if (name.indexOf('名') !== -1) {
    value = '@cname'
  }
  if (name.indexOf('邮政') !== -1) {
    value = '@zip'
  }
  if (name.indexOf('码') !== -1) {
    value = '@integer(100000 , 999999)'
  }
  if (name.indexOf('号码') !== -1) {
    value = /^1[385][1-9]\d{8}/
  }
  if (name.indexOf('单位') !== -1) {
    value = '@ctitle(8)'
  }
  if (name.indexOf('验票结果') !== -1) {
    value = '未知真伪'
  }
  if (name.indexOf('承兑单位开户行') !== -1) {
    value = '工商银行北站支行'
  }
  if (name.indexOf('收款单位开户行') !== -1) {
    value = '工商银行北站支行'
  }
  return value
}

function dataDemo(datamodel, treeflag) {
  const List = []
  const dict = {}
  for (let i = 0; i < count; i++) {
    for (const item in datamodel.list) {
      if (datamodel.list[item].type === 'grid') {
        for (const col in datamodel.list[item].columns) {
          for (const citem in datamodel.list[item].columns[col].list) {
            if (datamodel.list[item].columns[col].list[citem].type === 'date' || datamodel.list[item].columns[col].list[citem].type === 'time') {
              if (!datamodel.list[item].columns[col].list[citem].options.timestamp) {
                dict[datamodel.list[item].columns[col].list[citem].model] = '@DATETIME("' + datamodel.list[item].columns[col].list[citem].options.format + '")'
              } else {
                dict[datamodel.list[item].columns[col].list[citem].model] = +Mock.Random.date('T')
              }
            } else if (datamodel.list[item].columns[col].list[citem].type === 'input') {
              if (datamodel.list[item].columns[col].list[citem].options.dataType === 'number') {
                dict[datamodel.list[item].columns[col].list[citem].model] = '@integer(1, 3)'
              } else if (datamodel.list[item].columns[col].list[citem].options.dataType === 'integer') {
                dict[datamodel.list[item].columns[col].list[citem].model] = '@integer(1, 3)'
              } else if (datamodel.list[item].columns[col].list[citem].options.dataType === 'float') {
                dict[datamodel.list[item].columns[col].list[citem].model] = '@float(0, 100, 2, 2)'
              } else {
                dict[datamodel.list[item].columns[col].list[citem].model] = randomdata(datamodel.list[item].columns[col].list[citem].name)
              }
            } else if (datamodel.list[item].columns[col].list[citem].type === 'select') {
              const oplist = []
              if (datamodel.list[item].columns[col].list[citem].options.remote) {
                for (const op in getCodelists(datamodel.list[item].columns[col].list[citem].name)) {
                  oplist.push(getCodelists(datamodel.list[item].columns[col].list[citem].name)[op].label)
                }
              } else {
                for (const op in datamodel.list[item].columns[col].list[citem].options.options) {
                  oplist.push(datamodel.list[item].columns[col].list[citem].options.options[op].value)
                }
              }
              dict[datamodel.list[item].columns[col].list[citem].model + '|1'] = oplist
            } else if (datamodel.list[item].columns[col].list[citem].type === 'textarea') {
              dict[datamodel.list[item].columns[col].list[citem].model] = '@title(5, 10)'
            } else {
              // dict[datamodel.list[item].columns[col].list[citem].model] = []
            }
          }
        }
      } else {
        if (datamodel.list[item].type === 'date' || datamodel.list[item].type === 'time') {
          if (!datamodel.list[item].options.timestamp) {
            dict[datamodel.list[item].model] = '@DATETIME("' + datamodel.list[item].options.format + '")'
          } else {
            dict[datamodel.list[item].model] = +Mock.Random.date('T')
          }
        } else if (datamodel.list[item].type === 'input') {
          if (datamodel.list[item].options.dataType === 'number') {
            dict[datamodel.list[item].model] = '@integer(1, 3)'
          } else if (datamodel.list[item].options.dataType === 'integer') {
            dict[datamodel.list[item].model] = '@integer(1, 3)'
          } else if (datamodel.list[item].options.dataType === 'float') {
            dict[datamodel.list[item].model] = '@float(0, 100, 2, 2)'
          } else {
            dict[datamodel.list[item].model] = randomdata(datamodel.list[item].name)
          }
        } else if (datamodel.list[item].type === 'select') {
          const oplist = []
          if (datamodel.list[item].options.remote) {
            for (const op in getCodelists(datamodel.list[item].name)) {
              oplist.push(getCodelists(datamodel.list[item].name)[op].label)
            }
          } else {
            for (const op in datamodel.list[item].options.options) {
              oplist.push(datamodel.list[item].options.options[op].value)
            }
          }

          dict[datamodel.list[item].model + '|1'] = oplist
        } else if (datamodel.list[item].type === 'textarea') {
          dict[datamodel.list[item].model] = '@title(5, 10)'
        } else {
          // dict[datamodel.list[item].model] = []
        }
      }
    }
    if (treeflag) {
      dict['children'] = dataDemo(datamodel, false)
    }
    List.push(Mock.mock(dict))
  }
  return List
}

export default {
  getList: config => {
    const { importance, type, title, page = 1, limit = 20, sort, data, tree } = param2Obj(config.url)
    let treeflag = false
    if (tree) {
      treeflag = true
    }
    let mockList = dataDemo(JSON.parse(data), treeflag).filter(item => {
      if (importance && item.importance !== +importance) return false
      if (type && item.type !== type) return false
      if (title && item.title.indexOf(title) < 0) return false
      return true
    })

    if (sort === '-id') {
      mockList = mockList.reverse()
    }

    const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

    return {
      total: mockList.length,
      items: pageList
    }
  },
  getPv: () => ({
    pvData: [{ key: 'PC', pv: 1024 }, { key: 'mobile', pv: 1024 }, { key: 'ios', pv: 1024 }, { key: 'android', pv: 1024 }]
  }),
  getArticle: (config) => {
    const { id, data } = param2Obj(config.url)
    for (const article of dataDemo(JSON.parse(data))) {
      if (article.id === +id) {
        return article
      }
    }
  },
  createArticle: () => ({
    data: 'success'
  }),
  updateArticle: () => ({
    data: 'success'
  })
}
