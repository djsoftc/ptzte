/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const AccountModuleRouter = {
  path: '/AccountModule',
  component: Layout,
  redirect: '/PrivateLibrary/AccountModule',
  name: 'AccountModule',
  meta: {
    title: '会计模型',
    icon: 'nested'
  },
  children: [
    {
      path: 'ParameterDefinition',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'ParameterDefinition',
      redirect: '/PrivateLibrary/AccountModule/ParameterDefinition',
      meta: { title: '核算参数定义' },
      children: [
        {
          path: 'SupplierNewFile',
          component: () => import('@/views/PrivateLibrary/AccountModule/ParameterDefinition/SubjectDefinition'),
          name: 'SubjectDefinition',
          meta: { title: '科目定义' }
        },
        {
          path: 'ProductAcctDefinistion',
          component: () => import('@/views/PrivateLibrary/AccountModule/ParameterDefinition/ProductAcctDefinistion'),
          name: 'ProductAcctDefinistion',
          meta: { title: '产品科目组对照关系定义' }
        },
        {
          path: 'ProductEntryDefinition',
          component: () => import('@/views/PrivateLibrary/AccountModule/ParameterDefinition/ProductEntryDefinition'),
          name: 'ProductEntryDefinition',
          meta: { title: '产品事件分录定义' }
        }
      ]
    },
    {
      path: 'AccountManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'AccountManagement',
      redirect: '/PrivateLibrary/AccountModule/AccountManagement',
      meta: { title: '账户管理' },
      children: [
        {
          path: 'InterAcctCtrDefinition',
          component: () => import('@/views/PrivateLibrary/AccountModule/AccountManagement/InterAcctCtrDefinition'),
          name: 'InterAcctCtrDefinition',
          meta: { title: '内部核算账户控制表定义' }
        },
        {
          path: 'InterAcctDefinition',
          component: () => import('@/views/PrivateLibrary/AccountModule/AccountManagement/InterAcctDefinition'),
          name: 'InterAcctDefinition',
          meta: { title: '内部核算账户定义' }
        },
        {
          path: 'InterAcctBatch',
          /* component: () => import('@/views/PrivateLibrary/AccountModule/AccountManagement/InterAcctHangingDefinition'),*/
          component: () => import('@/views/PrivateLibrary/AccountModule/AccountManagement/InterAcctBatch'),
          name: 'InterAcctBatch',
          meta: { title: '内部核算账户批量销账' }
        },
        {
          path: 'InterAcctHangingDefinition',
          component: () => import('@/views/PrivateLibrary/AccountModule/AccountManagement/InterAcctHangingDefinition'),
          name: 'InterAcctHangingDefinition',
          meta: { title: '内部核算账户挂销账管理' }
        }
      ]
    },
    {
      path: 'QueryManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'QueryManagement',
      redirect: '/PrivateLibrary/AccountModule/QueryManagement',
      meta: { title: '查询管理' },
      children: [
        {
          path: 'SubjectQuery',
          component: () => import('@/views/PrivateLibrary/AccountModule/QueryManagement/SubjectQuery'),
          name: 'SubjectQuery',
          meta: { title: '科目查询' }
        },
        {
          path: 'SummonsQuery',
          component: () => import('@/views/PrivateLibrary/AccountModule/QueryManagement/SummonsQuery'),
          name: 'SummonsQuery',
          meta: { title: '传票信息查询' }
        },
        {
          path: 'SummonsExceptionQuery',
          component: () => import('@/views/PrivateLibrary/AccountModule/QueryManagement/SummonsExceptionQuery'),
          name: 'SummonsExceptionQuery',
          meta: { title: '传票异常信息查询' }
        },
        {
          path: 'InterAcctQuery',
          component: () => import('@/views/PrivateLibrary/AccountModule/QueryManagement/InterAcctQuery'),
          name: 'InterAcctQuery',
          meta: { title: '内部核算账户交易明细查询' }
        },
        {
          path: 'InterAcctHangingQuery',
          component: () => import('@/views/PrivateLibrary/AccountModule/QueryManagement/InterAcctHangingQuery'),
          name: 'InterAcctHangingQuery',
          meta: { title: '内部核算账户挂销账查询' }
        }
      ]
    }
  ]
}
export default AccountModuleRouter
