/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const InquireRouter = {
  path: '/Inquire',
  component: Layout,
  redirect: '/PrivateLibrary/Inquire',
  name: 'Inquire',
  meta: {
    title: '查询管理',
    icon: 'table'
  },
  children: [
    {
      path: 'OverheadDoor',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'OverheadDoor',
      redirect: '/PrivateLibrary/Inquire/OverheadDoor',
      meta: { title: '开销户登记表查询' },
      children: [
        {
          path: 'Inquire',
          component: () => import('@/views/PrivateLibrary/Inquire/OverheadDoor/Inquire'),
          name: 'Inquire',
          meta: { title: '开销户登记查询' }
        }
      ]
    },
    {
      path: 'InformationInquire',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'InformationInquire',
      redirect: '/PrivateLibrary/Inquire/InformationInquire',
      meta: { title: '账户信息查询' },
      children: [
        {
          path: 'InformationInquire',
          component: () => import('@/views/PrivateLibrary/Inquire/InformationInquire/InformationInquire'),
          name: 'InformationInquire',
          meta: { title: '账户信息查询' }
        }
      ]
    },
    {
      path: 'FuzzyQuery',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'FuzzyQuery',
      redirect: '/PrivateLibrary/Inquire/FuzzyQuery',
      meta: { title: '账户模糊查询' },
      children: [
        {
          path: 'FuzzyQuery',
          component: () => import('@/views/PrivateLibrary/Inquire/FuzzyQuery/FuzzyQuery'),
          name: 'FuzzyQuery',
          meta: { title: '账户模糊查询' }
        }
      ]
    }
  ]
}
export default InquireRouter
