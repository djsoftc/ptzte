/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const PledgeApplyRouter = {
  path: '/PledgeApply',
  component: Layout,
  redirect: '/PrivateLibrary/PledgeApply/PledgeApply',
  name: 'PledgeApply',
  meta: {
    title: '质押申请',
    icon: 'table'
  },
  children: [
    {
      path: 'PledgeApply',
      component: () => import('@/views/PrivateLibrary/PledgeApply/PledgeApply'),
      name: 'PledgeApply',
      meta: { title: '质押申请' }
    }
  ]
}

export default PledgeApplyRouter
