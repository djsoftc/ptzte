import Layout from '@/views/layout/Layout'

const LoanRouter = {
  path: '/Loan',
  component: Layout,
  redirect: '/PrivateLibrary/Loan',
  name: 'Loan',
  meta: {
    title: '贷款',
    icon: 'table'
  },
  children: [
    {
      path: 'ProductFactory',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'ProductFactory',
      redirect: '/PrivateLibrary/Loan/ProductFactory',
      meta: { title: '产品工厂' },
      children: [
        {
          path: 'ProductCopy',
          component: () => import('@/views/PrivateLibrary/Loan/ProductFactory/ProductCopy'),
          name: 'ProductCopy',
          meta: { title: '产品拷贝' }
        },
        {
          path: 'LoanProductMaint',
          component: () => import('@/views/PrivateLibrary/Loan/ProductFactory/LoanProductMaint'),
          name: 'LoanProductMaint',
          meta: { title: '产品维护' }
        },
        {
          path: 'ProductDeletion',
          component: () => import('@/views/PrivateLibrary/Loan/ProductFactory/ProductDeletion'),
          name: 'ProductDeletion',
          meta: { title: '产品删除' }
        },
        {
          path: 'ProductQuery',
          component: () => import('@/views/PrivateLibrary/Loan/ProductFactory/ProductQuery'),
          name: 'ProductQuery',
          meta: { title: '茶品查询' }
        }
      ]
    },
    {
      path: 'AccountManag',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'AccountManag',
      redirect: '/PrivateLibrary/Loan/AccountManag',
      meta: { title: '账户管理' },
      children: [
        {
          path: 'LoanAccountOpening',
          component: () => import('@/views/PrivateLibrary/Loan/AccountManag/LoanAccountOpening'),
          name: 'LoanAccountOpening',
          meta: { title: '贷款开户' }
        },
        {
          path: 'TransferLoanForm',
          component: () => import('@/views/PrivateLibrary/Loan/AccountManag/TransferLoanForm'),
          name: 'TransferLoanForm',
          meta: { title: '贷款形态转移' }
        },
        {
          path: 'LoanFormReversal',
          component: () => import('@/views/PrivateLibrary/Loan/AccountManag/LoanFormReversal'),
          name: 'LoanFormReversal',
          meta: { title: '贷款形态逆转' }
        }
      ]
    },
    {
      path: 'ChangeManag',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'ChangeManag',
      redirect: '/PrivateLibrary/Loan/ChangeManag',
      meta: { title: '变更管理' },
      children: [
        {
          path: 'LoanDataMain',
          component: () => import('@/views/PrivateLibrary/Loan/ChangeManag/LoanDataMain'),
          name: 'LoanDataMain',
          meta: { title: '贷款资料维护' }
        },
        {
          path: 'MaintenanceRepayment',
          component: () => import('@/views/PrivateLibrary/Loan/ChangeManag/MaintenanceRepayment'),
          name: 'MaintenanceRepayment',
          meta: { title: '还款资料维护' }
        },
        {
          path: 'LoanProductChange',
          component: () => import('@/views/PrivateLibrary/Loan/ChangeManag/LoanProductChange'),
          name: 'LoanProductChange',
          meta: { title: '贷款产品变更' }
        },
        {
          path: 'CancellationInstruction',
          component: () => import('@/views/PrivateLibrary/Loan/ChangeManag/CancellationInstruction'),
          name: 'CancellationInstruction',
          meta: { title: '录入指令取消' }
        },
        {
          path: 'LoanExtension',
          component: () => import('@/views/PrivateLibrary/Loan/ChangeManag/LoanExtension'),
          name: 'LoanExtension',
          meta: { title: '贷款展期' }
        },
        {
          path: 'LoanContractionPeriod',
          component: () => import('@/views/PrivateLibrary/Loan/ChangeManag/LoanContractionPeriod'),
          name: 'LoanContractionPeriod',
          meta: { title: '贷款缩期' }
        }
      ]
    },
    {
      path: 'FinTransactions',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'FinTransactions',
      redirect: '/PrivateLibrary/Loan/FinTransactions/LoanIssuance',
      meta: { title: '金融交易' },
      children: [
        {
          path: 'LoanIssuance',
          component: () => import('@/views/PrivateLibrary/Loan/FinTransactions/LoanIssuance'),
          name: 'LoanIssuance',
          meta: { title: '贷款发放' }
        },
        {
          path: 'LoanRepayment',
          component: () =>
            import('@/views/PrivateLibrary/PublicRouter'),
          name: 'FinTransactions',
          redirect: '/PrivateLibrary/Loan/FinTransactions',
          meta: { title: '贷款还款' },
          children: [
            {
              path: 'Repayment',
              component: () =>
              import('@/views/PrivateLibrary/Loan/FinTransactions/Repayment'),
              name: 'Repayment',
              meta: { title: '正常还款' }
            },
            {
              path: 'SpecialRepayment',
              component: () =>
                import('@/views/PrivateLibrary/Loan/FinTransactions/SpecialRepayment'),
              name: 'SpecialRepayment',
              meta: { title: '特殊贷款还款' }
            }
          ]
        },
        {
          path: 'LoanVerification',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'FinTransactions',
          redirect: '/PrivateLibrary/Loan/FinTransactions',
          meta: { title: '贷款核销' },
          children: [
            {
              path: 'BadDebtCancellation',
              component: () => import('@/views/PrivateLibrary/Loan/FinTransactions/BadDebtCancellation'),
              name: 'BadDebtCancellation',
              meta: { title: '呆账核销' }
            },
            {
              path: 'CancellationReturn',
              component: () =>
                import('@/views/PrivateLibrary/Loan/FinTransactions/CancellationReturn'),
              name: 'CancellationReturn',
              meta: { title: '核销归还' }
            }
          ]
        },
        {
          path: 'FinTransactions',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'FinTransactions',
          redirect: '/PrivateLibrary/Loan/FinTransactions',
          meta: { title: '贷款本息调账' },
          children: [{
            path: 'LoanPrincipalReconciliation',
            component: () => import('@/views/PrivateLibrary/Loan/FinTransactions/LoanPrincipalReconciliation'),
            name: 'LoanPrincipalReconciliation',
            meta: { title: '贷款本息调账' }
          }]
        },
        {
          path: 'FinTransactions',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'FinTransactions',
          redirect: '/PrivateLibrary/Loan/FinTransactions',
          meta: { title: '利率调整' },
          children: [{
            path: 'RateAdjustment',
            component: () => import('@/views/PrivateLibrary/Loan/FinTransactions/RateAdjustment'),
            name: 'RateAdjustment',
            meta: { title: '利率调整' }
          }]
        }
      ]
    },
    {
      path: 'LoanQryManagement',
      component: () =>
        import('@/views/PrivateLibrary/PublicRouter'),
      name: 'LoanQryManagement',
      redirect: '/PrivateLibrary/Loan/FinTransactions',
      meta: { title: '查询管理' },
      children: [{
        path: 'LoanInformationQry',
        component: () =>
          import('@/views/PrivateLibrary/Loan/FinTransactions/LoanInformationQry'),
        name: 'LoanInformationQry',
        meta: { title: '贷款信息查询' }
      },
      {
        path: 'LoanAccountTranQry',
        component: () =>
            import('@/views/PrivateLibrary/Loan/FinTransactions/LoanAccountTranQry'),
        name: 'LoanAccountTranQry',
        meta: { title: '贷款账户交易明细查询' }
      },
      {
        path: 'RateAdjustQry',
        component: () =>
            import('@/views/PrivateLibrary/Loan/FinTransactions/RateAdjustQry'),
        name: 'RateAdjustQry',
        meta: { title: '利率变更明细查询' }
      },
      {
        path: 'LoanStatusTranQry',
        component: () =>
            import('@/views/PrivateLibrary/Loan/FinTransactions/LoanStatusTranQry'),
        name: 'RateAdjustQry',
        meta: { title: '贷款形态转移明细查询' }
      },
      {
        path: 'LoanMaturityQry',
        component: () =>
            import('@/views/PrivateLibrary/Loan/FinTransactions/LoanMaturityQry'),
        name: 'LoanMaturityQry',
        meta: { title: '贷款期供明细查询' }
      }
      ]
    },
    {
      path: 'PledgeManagement',
      component: () =>
        import('@/views/PrivateLibrary/PublicRouter'),
      name: 'PledgeManagement',
      redirect: '/PrivateLibrary/Loan/FinTransactions',
      meta: { title: '抵质押管理' },
      children: [{
        path: 'PledgeInto',
        component: () =>
          import('@/views/PrivateLibrary/Loan/FinTransactions/PledgeInto'),
        name: 'PledgeInto',
        meta: { title: '抵质押物入库' }
      },
      {
        path: 'PledgeOut',
        component: () =>
            import('@/views/PrivateLibrary/Loan/FinTransactions/PledgeOut'),
        name: 'PledgeOut',
        meta: { title: '抵质押物出库' }
      },
      {
        path: 'PledgeReturn',
        component: () =>
            import('@/views/PrivateLibrary/Loan/FinTransactions/PledgeReturn'),
        name: 'PledgeReturn',
        meta: { title: '质押强制还贷' }
      }
      ]
    }
  ]
}
export default LoanRouter
