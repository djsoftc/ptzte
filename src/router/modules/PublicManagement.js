/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const PublicManagementRouter = {
  path: '/PublicManagement',
  component: Layout,
  redirect: '/PrivateLibrary/PublicManagement',
  name: 'PublicManagement',
  meta: {
    title: '公共管理',
    icon: 'nested'
  },
  children: [
    {
      path: 'CustomerManag',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'CustomerManag',
      redirect: '/PrivateLibrary/PublicManagement/CustomerManag',
      meta: { title: '客户管理' },
      children: [
        {
          path: 'CustomerInforImport',
          component: () => import('@/views/PrivateLibrary/PublicManagement/CustomerManag/CustomerInforImport'),
          name: 'CustomerInforImport',
          meta: { title: '客户信息导入' }
        },
        {
          path: 'ManualRegistration',
          component: () => import('@/views/PrivateLibrary/PublicManagement/CustomerManag/ManualRegistration'),
          name: 'ManualRegistration',
          meta: { title: '客户信息手工登记' }
        },
        {
          path: 'CustomerInfoMain',
          component: () => import('@/views/PrivateLibrary/PublicManagement/CustomerManag/CustomerInfoMain'),
          name: 'CustomerInfoMain',
          meta: { title: '客户信息维护' }
        }
      ]
    },
    {
      path: 'InterestRateMain',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'InterestRateMain',
      redirect: '/PrivateLibrary/PublicManagement/InterestRateMain',
      meta: { title: '利率管理' },
      children: [
        {
          path: 'MarketInterestRate',
          component: () => import('@/views/PrivateLibrary/PublicManagement/InterestRateMain/MarketInterestRate'),
          name: 'MarketInterestRate',
          meta: { title: '市场利率导入' }
        },
        {
          path: 'InterestRateCopy',
          component: () => import('@/views/PrivateLibrary/PublicManagement/InterestRateMain/InterestRateCopy'),
          name: 'InterestRateCopy',
          meta: { title: '利率拷贝' }
        },
        {
          path: 'InterestRateMain',
          component: () => import('@/views/PrivateLibrary/PublicManagement/InterestRateMain/InterestRateMain'),
          name: 'InterestRateMain',
          meta: { title: '利率维护' }
        },
        {
          path: 'InterestRateScheme',
          component: () => import('@/views/PrivateLibrary/PublicManagement/InterestRateMain/InterestRateScheme'),
          name: 'InterestRateScheme',
          meta: { title: '利率优惠计划维护' }
        },
        {
          path: 'DimensionMaint',
          component: () => import('@/views/PrivateLibrary/PublicManagement/InterestRateMain/DimensionMaint'),
          name: 'DimensionMaint',
          meta: { title: '利率优惠维度维护' }
        }
      ]
    },
    {
      path: 'VoucherManag',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'VoucherManag',
      redirect: '/PrivateLibrary/PublicManagement/VoucherManag',
      meta: { title: '凭证管理' },
      children: [
        {
          path: 'Voucher',
          component: () => import('@/views/PrivateLibrary/PublicManagement/VoucherManag/Voucher'),
          name: 'Voucher',
          meta: { title: '凭证入库' }
        },
        {
          path: 'VoucherTreasury',
          component: () => import('@/views/PrivateLibrary/PublicManagement/VoucherManag/VoucherTreasury'),
          name: 'VoucherTreasury',
          meta: { title: '凭证出库' }
        },
        {
          path: 'CertificateWithdrawal',
          component: () => import('@/views/PrivateLibrary/PublicManagement/VoucherManag/CertificateWithdrawal'),
          name: 'CertificateWithdrawal',
          meta: { title: '凭证出入库撤销' }
        }
      ]
    }
  ]
}
export default PublicManagementRouter
