/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const CapitalAndCloseRouter = {
  path: '/PrivateLibrary',
  component: Layout,
  redirect: '/PrivateLibrary/CapitalAndClose/AccountManage',
  name: 'PrivateLibrary',
  meta: {
    title: '资金集中与结算管理',
    icon: 'nested'
  },
  children: [
    {
      path: 'CapitalAndClose',
      component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
      name: 'CapitalAndClose',
      meta: { title: '账户管理' },
      redirect: '/PrivateLibrary/CapitalAndClose/AccountManage',
      children: [
        {
          path: 'AccountManage',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'AccountManage',
          redirect: '/PrivateLibrary/CapitalAndClose/AccountManage/AccountApply',
          meta: { title: '账户开立' },
          children: [
            {
              path: 'AccountApply',
              component: () => import('@/views/PrivateLibrary/CapitalAndClose/AccountManage/AccountApply'),
              name: 'AccountApply',
              meta: { title: '账户开立申请' }
            },
            {
              path: 'ExternalAccountOpening',
              component: () => import('@/views/PrivateLibrary/CapitalAndClose/AccountManage/ExternalAccountOpening'),
              name: 'ExternalAccountOpening',
              meta: { title: '外部账户开立' }
            },
            {
              path: 'InternalAccountOpening',
              component: () => import('@/views/PrivateLibrary/CapitalAndClose/AccountManage/InternalAccountOpening'),
              name: 'InternalAccountOpening',
              meta: { title: '内部账户开立' }
            }
          ]
        },
        {
          path: 'AccountCancellation',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'AccountCancellation',
          redirect: '/PrivateLibrary/CapitalAndClose/AccountCancellation/AccountCancellationApply',
          meta: { title: '账户注销' },
          children: [
            {
              path: 'AccountCancellationApply',
              component: () => import('@/views/PrivateLibrary/CapitalAndClose/AccountCancellation/AccountCancellationApply'),
              name: 'AccountCancellationApply',
              meta: { title: '账户销户申请' }
            },
            {
              path: 'AccountCancellation',
              component: () => import('@/views/PrivateLibrary/CapitalAndClose/AccountCancellation/AccountCancellation'),
              name: 'AccountCancellation',
              meta: { title: '账户注销' }
            }
          ]
        }
      ]
    },
    {
      path: 'CapitalPoolManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
      name: 'CapitalPoolManagement',
      meta: { title: '资金池管理' },
      redirect: '/PrivateLibrary/CapitalPoolManagement/GroupRelationshipManagement',
      children: [
        {
          path: 'GroupRelationshipManagement',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'GroupRelationshipManagement',
          redirect: '/PrivateLibrary/CapitalPoolManagement/GroupRelationshipManagement/GroupRelationsRegistration',
          meta: { title: '集团关系管理' },
          children: [
            {
              path: 'GroupRelationsRegistration',
              component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/GroupRelationshipManagement/GroupRelationsRegistration'),
              name: 'GroupRelationsRegistration',
              meta: { title: '集团关系登记' }
            },
            {
              path: 'RelationshipDissolution',
              component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/GroupRelationshipManagement/RelationshipDissolution'),
              name: 'RelationshipDissolution',
              meta: { title: '集团关系解除' }
            },
            {
              path: 'GrounpInquire',
              component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/GroupRelationshipManagement/GrounpInquire'),
              name: 'GrounpInquire',
              meta: { title: '集团关系查询结果' }
            }
          ]
        },
        {
          path: 'ResourcesManagement',
          component: () => import('@/views/PrivateLibrary/PublicRouter'),
          name: 'ResourcesManagement',
          redirect: '/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsSetUp',
          meta: { title: '资金调拨管理' },
          children: [
            {
              path: 'FundsSetUp',
              component: () => import('@/views/PrivateLibrary/PublicRouter'),
              name: 'FundsSetUp',
              redirect: '/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsSetUp/FundsSetUp',
              meta: { title: '资金归集设置' },
              children: [
                {
                  path: 'FundsSetUp',
                  component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsSetUp/FundsSetUp'),
                  name: 'FundsSetUp',
                  meta: { title: '资金归集设置' }
                },
                {
                  path: 'AccountInformation',
                  component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsSetUp/AccountInformation'),
                  name: 'AccountInformation',
                  meta: { title: '账户信息' }
                }
              ]
            },
            {
              path: 'FundsAllocated',
              component: () => import('@/views/PrivateLibrary/PublicRouter'),
              name: 'FundsAllocated',
              redirect: '/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsAllocated/FundsSetUp',
              meta: { title: '资金归集设置' },
              children: [
                {
                  path: 'FundsSetUp',
                  component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsAllocated/FundsSetUp'),
                  name: 'FundsSetUp',
                  meta: { title: '资金下拨设置' }
                },
                {
                  path: 'AccountInformation',
                  component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/FundsAllocated/AccountInformation'),
                  name: 'AccountInformation',
                  meta: { title: '账户信息' }
                }
              ]
            },
            {
              path: 'Allot',
              component: () => import('@/views/PrivateLibrary/PublicRouter'),
              name: 'Allot',
              redirect: '/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/Allot/AllotList',
              meta: { title: '资金调拨维护' },
              children: [
                {
                  path: 'AllotList',
                  component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/Allot/AllotList'),
                  name: 'AllotList',
                  meta: { title: '资金调拨维护' }
                }
              ]
            },
            {
              path: 'GrounpAccount',
              component: () => import('@/views/PrivateLibrary/CapitalPoolManagement/ResourcesManagement/GrounpAccount'),
              name: 'GrounpAccount',
              meta: { title: '集团账户关系查询' }
            }
          ]
        }
      ]
    },
    {
      path: 'ReceivablesPayment',
      component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
      name: 'ReceivablesPayment',
      meta: { title: '收付款管理' },
      redirect: '/PrivateLibrary/ReceivablesPayment/Receivables',
      children: [
        {
          path: 'Receivables',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'Receivables',
          meta: { title: '收款管理' },
          redirect: '/PrivateLibrary/ReceivablesPayment/Receivables/ReturnReceipt',
          children: [
            {
              path: 'ReturnReceipt',
              component: () => import('@/views/PrivateLibrary/ReceivablesPayment/Receivables/ReturnReceipt'),
              name: 'ReturnReceipt',
              meta: { title: '回单获取' }
            },
            {
              path: 'ReceiptReceiptProcessing',
              component: () => import('@/views/PrivateLibrary/ReceivablesPayment/Receivables/ReceiptReceiptProcessing'),
              name: 'ReceiptReceiptProcessing',
              meta: { title: '收款回单处理' }
            }
          ]
        },
        {
          path: 'PaymentMana',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'PaymentMana',
          meta: { title: '付款管理' },
          redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/VoucherMana',
          children: [
            {
              path: 'VoucherMana',
              component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
              name: 'VoucherMana',
              meta: { title: '凭证管理' },
              redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/VoucherMana/VoucherExtraction',
              children: [
                {
                  path: 'VoucherExtraction',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/VoucherMana/VoucherExtraction'),
                  name: 'VoucherExtraction',
                  meta: { title: '凭证提取' }
                },
                {
                  path: 'VoucherInquire',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/VoucherMana/VoucherInquire'),
                  name: 'VoucherInquire',
                  meta: { title: '凭证查询' }
                },
                {
                  path: 'VoucherDetailedInquiry',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/VoucherMana/VoucherDetailedInquiry'),
                  name: 'VoucherDetailedInquiry',
                  meta: { title: '凭证明细查询' }
                }
              ]
            },
            {
              path: 'PaymentPlan',
              component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
              name: 'PaymentPlan',
              meta: { title: '付款计划' },
              redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentPlan/PaymentPlanNew',
              children: [
                {
                  path: 'PaymentPlanNew',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentPlan/PaymentPlanNew'),
                  name: 'PaymentPlanNew',
                  meta: { title: '付款计划新增' }
                },
                {
                  path: 'PaymentPlanMaintenance',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentPlan/PaymentPlanMaintenance'),
                  name: 'PaymentPlanMaintenance',
                  meta: { title: '付款计划维护' }
                },
                {
                  path: 'PayPlanDetaitedInquire',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentPlan/PayPlanDetaitedInquire'),
                  name: 'PayPlanDetaitedInquire',
                  meta: { title: '付款计划明细查询' }
                }
              ]
            },
            {
              path: 'PaymentApply',
              component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
              name: 'PaymentApply',
              meta: { title: '付款申请' },
              redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PaymentApplyNew',
              children: [
                {
                  path: 'PaymentApplyNew',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PaymentApplyNew'),
                  name: 'PaymentApplyNew',
                  meta: { title: '付款申请新增' }
                },
                {
                  path: 'PaymentApplyPreview',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PaymentApplyPreview'),
                  name: 'PaymentApplyPreview',
                  meta: { title: '付款申请单预览' },
                  hidden: true
                },
                {
                  path: 'PaymentApplyInquire',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PaymentApplyInquire'),
                  name: 'PaymentApplyInquire',
                  meta: { title: '付款申请查询' }
                },
                {
                  path: 'PayApplyMaintenance',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PayApplyMaintenance'),
                  name: 'PayApplyMaintenance',
                  meta: { title: '付款申请单维护' }
                },
                {
                  path: 'PayApplyListDetaileInquire',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PayApplyListDetaileInquire'),
                  name: 'PayApplyListDetaileInquire',
                  meta: { title: '付款申请查询明细' },
                  hidden: true
                },
                {
                  path: 'PayAppMainPreview',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PayAppMainPreview'),
                  name: 'PayAppMainPreview',
                  meta: { title: '付款申请单维护预览' },
                  hidden: true
                },
                {
                  path: 'AddSuboptions',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/AddSuboptions'),
                  name: 'AddSuboptions',
                  meta: { title: '付款项增加' },
                  hidden: true
                },
                {
                  path: 'CancelPpaymentApply',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/CancelPpaymentApply'),
                  name: 'CancelPpaymentApply',
                  meta: { title: '付款申请单撤销' }
                },
                {
                  path: 'PayAppDetailListInquire',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/PayAppDetailListInquire'),
                  name: 'PayAppDetailListInquire',
                  meta: { title: '付款申请单明细查询' }
                }
              ]
            },
            {
              path: 'PaymentApproval',
              component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
              name: 'PaymentApproval',
              meta: { title: '付款审批' },
              redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApply/ReviewPaymentApply',
              children: [
                {
                  path: 'ReviewPaymentApply',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApproval/ReviewPaymentApply'),
                  name: 'ReviewPaymentApply',
                  meta: { title: '付款申请单复核' }
                },
                {
                  path: 'ApprovalPaymentApply',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApproval/ApprovalPaymentApply'),
                  name: 'ApprovalPaymentApply',
                  meta: { title: '付款申请单审批' }
                },
                {
                  path: 'PayApproval',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApproval/PayApproval'),
                  name: 'PayApproval',
                  meta: { title: '付款项审批' }
                },
                {
                  path: 'PaySupervisorApprly',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApproval/PaySupervisorApprly'),
                  name: 'PaySupervisorApprly',
                  meta: { title: '付款款项主管审批' }
                },
                {
                  path: 'PayInqire',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PaymentApproval/PayInqire'),
                  name: 'PayInqire',
                  meta: { title: '付款项查询' }
                }
              ]
            },
            {
              path: 'PayManag',
              component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
              name: 'PayManag',
              meta: { title: '支付管理' },
              redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/PayManag/PaymentmMain',
              children: [
                {
                  path: 'PaymentmMain',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PayManag/PaymentmMain'),
                  name: 'PaymentmMain',
                  meta: { title: '支付维护' }
                },
                {
                  path: 'PayDeatailed',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PayManag/PayDeatailed'),
                  name: 'PayDeatailed',
                  meta: { title: '支付明细' },
                  hidden: true
                },
                {
                  path: 'SinglePayment',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PayManag/SinglePayment'),
                  name: 'SinglePayment',
                  meta: { title: '单一支付项处理' }
                },
                {
                  path: 'PayInformationSupp',
                  component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/PayManag/PayInformationSupp'),
                  name: 'PayInformationSupp',
                  meta: { title: '支付信息补录' }
                }
              ]
            }
          ]
        },
        {
          path: 'ComprehensiveQuery',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'ComprehensiveQuery',
          meta: { title: '综合查询' },
          redirect: '/PrivateLibrary/ReceivablesPayment/PaymentMana/ComprehensiveQuery',
          children: [
            {
              path: 'ScheduleEnquiryPaymentPlan',
              component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/ComprehensiveQuery/ScheduleEnquiryPaymentPlan'),
              name: 'ScheduleEnquiryPaymentPlan',
              meta: { title: '付款计划进度查询' }
            },
            {
              path: 'SecheduleEnquiryDetailed',
              component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/ComprehensiveQuery/SecheduleEnquiryDetailed'),
              name: 'SecheduleEnquiryDetailed',
              meta: { title: '付款计划进度明细' },
              hidden: true
            },
            {
              path: 'PayAppListInquire',
              component: () => import('@/views/PrivateLibrary/ReceivablesPayment/PaymentMana/ComprehensiveQuery/PayAppListInquire'),
              name: 'PayAppListInquire',
              meta: { title: '付款申请单进度查询' }
            }
          ]
        }
      ]
    },
    {
      path: 'BillManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
      name: 'BillManagement',
      meta: { title: '票据管理' },
      redirect: '/PrivateLibrary/BillManagement/BillOverview',
      children: [
        {
          path: 'BillOverview',
          component: () => import('@/views/PrivateLibrary/BillManagement/BillOverview'),
          name: 'BillOverview',
          meta: { title: '票据管理概览' }
        },
        {
          path: 'PayableManagement',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'PayableManagement',
          meta: { title: '应付票据业务' },
          redirect: '/PrivateLibrary/BillManagement/PayableManagement/Billing',
          children: [
            {
              path: 'Billing',
              component: () => import('@/views/PrivateLibrary/PublicRouter'),
              name: 'Billing',
              redirect: '/PrivateLibrary/BillManagement/PayableManagement/BillingApply',
              meta: { title: '开票' },
              children: [
                {
                  path: 'BillingApply',
                  component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/BillingApply'),
                  name: 'BillingApply',
                  meta: { title: '开票申请' }
                },
                {
                  path: 'Register',
                  component: () => import('@/views/PrivateLibrary/PublicRouter'),
                  name: 'Billing',
                  redirect: '/PrivateLibrary/BillManagement/PayableManagement/Billing/BillingRegister',
                  meta: { title: '开票登记' },
                  children: [
                    {
                      path: 'BillingRegister',
                      component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/Register/BillingRegister'),
                      name: 'BillingRegister',
                      meta: { title: '开票登记' }
                    },
                    {
                      path: 'ManualRegistration',
                      component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/Register/ManualRegistration'),
                      name: 'ManualRegistration',
                      meta: { title: '手工登记票据' }
                    },
                    {
                      path: 'ImportBill',
                      component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/Register/ImportBill'),
                      name: 'ImportBill',
                      meta: { title: '导入票据' }
                    }
                  ]
                },
                {
                  path: 'ElectronicBilling',
                  component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/ElectronicBilling'),
                  name: 'ElectronicBilling',
                  meta: { title: '电子开票' }
                },
                {
                  path: 'PromptCollectingTickets',
                  component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/PromptCollectingTickets'),
                  name: 'PromptCollectingTickets',
                  meta: { title: '提示收票' }
                },
                {
                  path: 'WithdrawClaim',
                  component: () => import('@/views/PrivateLibrary/BillManagement/PayableManagement/Billing/WithdrawClaim'),
                  name: 'WithdrawClaim',
                  meta: { title: '撤销申请' }
                }
              ]
            },
            {
              path: 'Enxashment',
              component: () => import('@/views/PrivateLibrary//BillManagement/PayableManagement/Enxashment'),
              name: 'Enxashment',
              meta: { title: '兑付' }
            }
          ]
        },
        {
          path: 'NotesReceivable',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'NotesReceivable',
          meta: { title: '应收票据业务' },
          redirect: '/PrivateLibrary/BillManagement/NotesReceivable/invoice',
          children: [
            {
              path: 'ElectronicSign',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/invoice/ElectronicSign'),
              name: 'ElectronicSign',
              meta: { title: '电子签收' }
            },
            {
              path: 'HandworkRegister',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/invoice/HandworkRegister'),
              name: 'HandworkRegister',
              meta: { title: '手工收票登记' }
            },
            {
              path: 'WarehousingApplication',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/invoice/WarehousingApplication'),
              name: 'WarehousingApplication',
              meta: { title: '入库申请' }
            },
            {
              path: 'AuthenticationTicket',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/invoice/AuthenticationTicket'),
              name: 'AuthenticationTicket',
              meta: { title: '票据验证' }
            }
          ]
        },
        {
          path: 'RefundApplication',
          component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/RefundApplication'),
          name: 'RefundApplication',
          meta: { title: '退票申请' }
        },
        {
          path: 'Negotiablity',
          component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/Negotiablity'),
          name: 'Negotiablity',
          meta: { title: '背书转让' }
        },
        {
          path: 'DiscountManagent',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'DiscountManagent',
          meta: { title: '贴现管理' },
          redirect: '/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent',
          children: [
            {
              path: 'DiscountApply',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent/DiscountApply'),
              name: 'DiscountApply',
              meta: { title: '贴现申请' }
            },
            {
              path: 'DiscountApplyInquire',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent/DiscountApplyInquire'),
              name: 'DiscountApplyInquire',
              meta: { title: '贴现申请查询' }
            },
            {
              path: 'DiscountTransaction',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent/DiscountTransaction'),
              name: 'DiscountTransaction',
              meta: { title: '贴现办理' }
            },
            {
              path: 'DiscountAccount',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent/DiscountAccount'),
              name: 'DiscountAccount',
              meta: { title: '贴现到账' }
            },
            {
              path: 'DiscountRedeemApply',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent/DiscountRedeemApply'),
              name: 'DiscountRedeemApply',
              meta: { title: '贴现赎回申请' }
            },
            {
              path: 'DiscountedRedemption',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/DiscountManagent/DiscountedRedemption'),
              name: 'DiscountedRedemption',
              meta: { title: '贴现赎回' }
            }
          ]
        },
        {
          path: 'PledgeManagement',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'PledgeManagement',
          meta: { title: '质押管理' },
          redirect: '/PrivateLibrary/BillManagement/NotesReceivable/PledgeManagement',
          children: [
            {
              path: 'DiscountedRedemption',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/PledgeManagement/ApplicationPledge'),
              name: 'DiscountedRedemption',
              meta: { title: '质押申请' }
            },
            {
              path: 'Pledge',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/PledgeManagement/Pledge'),
              name: 'Pledge',
              meta: { title: '质押办理' }
            },
            {
              path: 'ReleasePledge',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/PledgeManagement/ReleasePledge'),
              name: 'ReleasePledge',
              meta: { title: '解除质押' }
            }
          ]
        },
        {
          path: 'TrusteeshipManagement',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'TrusteeshipManagement',
          meta: { title: '托收管理' },
          redirect: '/PrivateLibrary/BillManagement/NotesReceivable/TrusteeshipManagement',
          children: [
            {
              path: 'ApplicationCollection',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/TrusteeshipManagement/ApplicationCollection'),
              name: 'ApplicationCollection',
              meta: { title: '托收申请' }
            },
            {
              path: 'CollectionHandle',
              component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/TrusteeshipManagement/CollectionHandle'),
              name: 'CollectionHandle',
              meta: { title: '托收办理' }
            }
          ]
        },
        {
          path: 'Currency',
          component: () => import('@/views/PrivateLibrary/BillManagement/NotesReceivable/Currency'),
          name: 'Currency',
          meta: { title: '通用撤回' }
        }
      ]
    },
    {
      path: 'BankReconciliationProcessing',
      component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
      name: 'BankReconciliationProcessing',
      meta: { title: '银行对账处理' },
      redirect: '/PrivateLibrary/BankReconciliationProcessing/ImportBills',
      children: [
        {
          path: 'ImportBills',
          component: () => import('@/views/PrivateLibrary/BankReconciliationProcessing/ImportBills'),
          name: 'ImportBills',
          meta: { title: '对账单导入' }
        },
        {
          path: 'BankReconciliations',
          component: () => import('@/views/PrivateLibrary/PublicRouter'), // Parent router-view
          name: 'BankReconciliations',
          meta: { title: '银行对账处理' },
          redirect: '/PrivateLibrary/BankReconciliationProcessing/BankReconciliations/BankReconciliations',
          children: [
            {
              path: 'BankReconciliations',
              component: () => import('@/views/PrivateLibrary/BankReconciliationProcessing/BankReconciliations/BankReconciliations'),
              name: 'BankReconciliations',
              meta: { title: '银行对账' }
            }
          ]
        },
        {
          path: 'BalanceAdjustmentQuery',
          component: () => import('@/views/PrivateLibrary/BankReconciliationProcessing/BalanceAdjustmentQuery'),
          name: 'BalanceAdjustmentQuery',
          meta: { title: '余额调节表查询' }
        },
        {
          path: 'ManualRepairAccount',
          component: () => import('@/views/PrivateLibrary/BankReconciliationProcessing/ManualRepairAccount'),
          name: 'ManualRepairAccount',
          meta: { title: '手工冲补账' }
        }
      ]
    }
  ]
}
export default CapitalAndCloseRouter
