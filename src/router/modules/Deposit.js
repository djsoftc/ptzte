/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const DepositRouter = {

  path: '/DepositManagement',
  component: Layout,
  redirect: '/PrivateLibrary/Inquire/OverheadDoor',
  name: 'DepositManagement',
  meta: {
    title: '存款管理',
    icon: 'table'
  },
  children: [
    {
      path: 'Inquire',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'Inquire',
      redirect: '/PrivateLibrary/Inquire/OverheadDoor',
      meta: { title: '产品工厂' },
      children: [
        {
          path: 'factoryCopy',
          component: () => import('@/views/PrivateLibrary/DepositManagement/DepositFactory/factoryCopy/index'),
          name: 'factoryCopy',
          meta: { title: '产品拷贝' }
        },
        {
          path: 'factoryMaintain',
          component: () => import('@/views/PrivateLibrary/DepositManagement/DepositFactory/factoryMaintain/index'),
          name: 'factoryMaintain',
          meta: { title: '产品维护' }
        },
        {
          path: 'factorySearch',
          component: () => import('@/views/PrivateLibrary/DepositManagement/DepositFactory/factorySearch/index'),
          name: 'factorySearch',
          meta: { title: '产品查询' }
        }
      ]
    },
    {
      path: 'AccountManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'AccountManagement',
      redirect: '/PrivateLibrary/DepositManagement/AccountManagement',
      meta: { title: '账户管理' },
      children: [
        {
          path: 'AccountOpen',
          component: () => import('@/views/PrivateLibrary/DepositManagement/AccountManagement/AccountOpen/index'),
          name: 'AccountOpen',
          meta: { title: '账户开户' },
          children: [
            {
              path: 'indexh',
              component: () => import('@/views/PrivateLibrary/DepositManagement/AccountManagement/AccountOpen/indexh'),
              name: 'indexh',
              meta: { title: '活期存款账户开户' }
            },
            {
              path: 'indexd',
              component: () => import('@/views/PrivateLibrary/DepositManagement/AccountManagement/AccountOpen/indexd'),
              name: 'indexd',
              meta: { title: '定期存款账户开户' }
            }
          ]
        },
        {
          path: 'AccountDelete',
          component: () => import('@/views/PrivateLibrary/DepositManagement/AccountManagement/AccountDelete/index'),
          name: 'AccountDelete',
          meta: { title: '账户销户' }
        },
        {
          path: 'AccountUpdate',
          component: () => import('@/views/PrivateLibrary/DepositManagement/AccountManagement/AccountUpdate/index'),
          name: 'AccountUpdate',
          meta: { title: '账户变更' }
        }
      ]
    },
    {
      path: 'FinTranManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'FinTranManagement',
      redirect: '/PrivateLibrary/Inquire/OverheadDoor',
      meta: { title: '金融交易管理' },
      children: [
        {
          path: 'DepositAccountDep',
          component: () => import('@/views/PrivateLibrary/DepositManagement/FinTranManagement/DepositAccountDep'),
          name: 'DepositAccountDep',
          meta: { title: '存款账户存入' }
        },
        {
          path: 'DepositAccountDraw',
          component: () => import('@/views/PrivateLibrary/DepositManagement/FinTranManagement/DepositAccountDraw'),
          name: 'DepositAccountDraw',
          meta: { title: '存款账户支取' }
        },
        {
          path: 'ReguNotify',
          component: () => import('@/views/PrivateLibrary/DepositManagement/FinTranManagement/ReguNotify'),
          name: 'ReguNotify',
          meta: { title: '定期及通知部提' }
        },
        {
          path: 'DepositAccountTransfer',
          component: () => import('@/views/PrivateLibrary/DepositManagement/FinTranManagement/DepositAccountTransfer'),
          name: 'DepositAccountTransfer',
          meta: { title: '存款账户转账' }
        }
      ]
    },
    {
      path: 'ConReConManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'ConReConManagement',
      redirect: '/PrivateLibrary/DepositManagement/AccountManagement',
      meta: { title: '控制/解控管理' },
      children: [
        {
          path: 'AccountControl',
          component: () => import('@/views/PrivateLibrary/DepositManagement/ConReConManagement/AccountControl'),
          name: 'AccountControl',
          meta: { title: '账户控制' }
        },
        {
          path: 'AccountControlMaintenancecl',
          component: () => import('@/views/PrivateLibrary/DepositManagement/ConReConManagement/AccountControlMaintenance/ControlList'),
          name: 'AccountControlMaintenancecl',
          meta: { title: '账户控制维护' }
        }
      ]
    },
    {
      path: 'ConsultativeManagement',
      component: () => import('@/views/PrivateLibrary/PublicRouter'),
      name: 'ConsultativeManagement',
      redirect: '/PrivateLibrary/ConsultativeManagement/index',
      meta: { title: '协议管理' },
      children: [
        {
          path: 'NoticeDepositMaintenance',
          component: () => import('@/views/PrivateLibrary/DepositManagement/ConsultativeManagement/NoticeDepositMaintenance'),
          name: 'NoticeDepositMaintenance',
          meta: { title: '协定存款设置及维护' }
        },
        {
          path: 'AgreementDepositsMaintenance',
          component: () => import('@/views/PrivateLibrary/DepositManagement/ConsultativeManagement/AgreementDepositsMaintenance'),
          name: 'AgreementDepositsMaintenance',
          meta: { title: '通知存款通知及维护' }
        }
      ]
    }
  ]
}
export default DepositRouter
